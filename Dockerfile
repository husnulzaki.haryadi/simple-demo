FROM python:3.8
WORKDIR /app/

ENV HOST="0.0.0.0"
ENV PORT="8080"

COPY . /app/
RUN pip install -r requirements.txt && \
    chmod +x entrypoint.sh

EXPOSE $PORT
ENTRYPOINT exec ./entrypoint.sh
