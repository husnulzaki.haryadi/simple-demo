from flask import Flask
app = Flask(__name__)


def foo(x, y, z):
    _x = int(y)
    _d = x ** 2
    _o = "hi!"

    for i in range(z):
        _d += _x * 3
        if i % 3 == 0:
            _o += chr(_d)
        else:
            _x = _x + 2

    return _o


def alias():
    print("Halo")


@app.route("/deb")
def debug():
    x = 12
    y = '32'
    z = 6

    json = {"a": 1, "b": 2}

    #breakpoint()
    out = foo(x, y, z)

    return out


@app.route("/")
def hello():
    return "Hello World!"

if __name__ == "__main__":
    app.run()
