#!/bin/bash

# This is a simple script to test our Hello app.
#
# The script will try to get a response string from the app and compare it with the
#   expected response. If it failed to get a response that fits the expected string,
#   then the app has failed the test.

if [ "$(curl ${HOST}:${PORT})" == "Hello World!" ]; then
    exit 0
else
    exit 1
fi
