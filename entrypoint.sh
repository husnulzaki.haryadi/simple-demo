#!/bin/bash
#
# entrypoint.sh declares the commands that will be executed when the container is
#   started.

gunicorn --reload -b "${HOST}:${PORT}" hello:app
